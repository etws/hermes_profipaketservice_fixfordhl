== Описание
Есть 2 модуля от команды netresearch.
DHL Intraship: http://www.magentocommerce.com/magento-connect/dhl-intraship-7494.html
Hermes ProfiPaketService: http://www.magentocommerce.com/magento-connect/hermes-profipaketservice-3309.html

Они без проблем работают по отдельности, но из-за особенностей 
исходного кода их не возможно использовать одновременно
на одном сайте Magento.

Оба модуля делают rewrite одинаковых стандартных блоков Magento
что приводит к несовместимости.

Данный проект содержит изменённый код модуля Hermes_ProfitPaketService,
чтобы можно было использовать вместе с модулем DHL Intraship.

== Внимание
Сам по себе этот модуль не работает.
Если вы хотите использовать Hermes ProfiPaketService
без модуля DHL Intraship, то устанавливайте оригинальный модуль,
а не этот исправленный.


== Инсталяция
Отключите кэш и компиляцию.
Скопируйте все файлы из папки install в корневую папку вашего сайта.
Сделайте исправление в модуле DHL Intraship,
в файле app/code/community/Dhl/Intraship/etc/config.xml
закомментируйте строки внутри тэга <rewrite>

<global>
    <blocks>
        <adminhtml>
            <rewrite>
                <!-- <sales_order_shipment_create_tracking>Dhl_Intraship_Block_Adminhtml_Sales_Order_Shipment_Create_Tracking</sales_order_shipment_create_tracking>
                <sales_order_shipment_view_tracking>Dhl_Intraship_Block_Adminhtml_Sales_Order_Shipment_View_Tracking</sales_order_shipment_view_tracking>
                <sales_order_grid>Dhl_Intraship_Block_Adminhtml_Sales_Order_Grid</sales_order_grid>
                <sales_shipment_grid>Dhl_Intraship_Block_Adminhtml_Sales_Shipment_Grid</sales_shipment_grid> -->
            </rewrite>

