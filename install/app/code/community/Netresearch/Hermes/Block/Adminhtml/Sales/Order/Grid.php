<?php
/**
 * Netresearch Hermes
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to
 * newer versions in the future.
 *
 * @category    Netresearch
 * @package     Netresearch_Hermes
 * @copyright   Copyright (c) 2012 Netresearch GmbH & Co. KG (http://www.netresearch.de/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Netresearch_Hermes_Block_Adminhtml_Sales_Order_Grid
 *
 * @category    Netresearch
 * @package     Netresearch_Hermes
 * @author      Thomas Birke <thomas.birke@netresearch.de>
 */
class Netresearch_Hermes_Block_Adminhtml_Sales_Order_Grid extends Dhl_Intraship_Block_Adminhtml_Sales_Order_Grid
{
    /**
     * Constructor.
     *
     * @see   app/code/core/Mage/Adminhtml/Block/Sales/Order/Mage_Adminhtml_Block_Sales_Order_Grid#__construct()
     * return Netresearch_Hermes_Block_Adminhtml_Sales_Order_Grid
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('widget/grid.phtml');
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
    }

    public function getResetFilterButtonHtml(){
        return parent::getResetFilterButtonHtml();
    }

    /**
     * Modify real order id column.
     *
     * @see    app/code/core/Mage/Adminhtml/Block/Sales/Order/Mage_Adminhtml_Block_Sales_Order_Grid#_prepareColumns()
     * @return Netresearch_Hermes_Block_Adminhtml_Sales_Order_Grid
     */
    protected function _prepareColumns()
    {
        parent::_prepareColumns();
        $this->addColumn('real_order_id', array(
            'header'   => Mage::helper('sales')->__('Order #'),
            'width'    => '110px',
            'index'    => 'increment_id',
            'renderer' => 'hermes/adminhtml_sales_order_grid_renderer_icon',
        ));
        return $this;
    }

    /**
     * add massaction
     *
     * @see    app/code/core/Mage/Adminhtml/Block/Sales/Order/Mage_Adminhtml_Block_Sales_Order_Grid#_prepareMassaction()
     * @return Netresearch_Hermes_Block_Adminhtml_Sales_Order_Grid
     */
    protected function _prepareMassaction()
    {
        parent::_prepareMassaction();
        if (true === Mage::getModel('hermes/config')->isEnabled()) {
            /* provide parcel class selection for tier price merchants */
            if (Mage::getModel('hermes/config')->isTieredPriceMerchant()) {
                $parcelClasses = Mage::getModel('hermes/config')->getAllProductClassesasKeyValue();
                array_unshift($parcelClasses, array('value' => '', 'label' => Mage::helper('hermes')->__('use default')));
                $additionalOptions = array(
                    'parcelClass'   => array(
                        'name'      => 'parcelClass',
                        'type'      => 'select',
                        'label'     => Mage::helper('hermes')->__('Parcel class'),
                        'values'    => $parcelClasses
                    ));
            } else {
                $additionalOptions = array();
            }
            /* add checkbox to notify customer */
            $additionalOptions['notifyCustomer'] = array(
                'name'      => 'notifyCustomer',
                'type'      => 'checkbox',
                'label'     => Mage::helper('hermes')->__('Notify Customer by Email'),
            );

            $this->getMassactionBlock()->addItem('createshipment_order_hermes', array(
                'label'         => Mage::helper('hermes')->__('Create shipment(s) for Hermes'),
                'url'           => $this->getUrl('adminhtml/shipment/createShipments'),
                'additional'    => $additionalOptions
            ));
        }
        return $this;
    }
}
