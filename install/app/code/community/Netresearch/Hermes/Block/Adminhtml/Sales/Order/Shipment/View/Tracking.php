<?php

/**
 * Netresearch Hermes
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to
 * newer versions in the future.
 *
 * @category    Netresearch
 * @package     Netresearch_Hermes
 * @copyright   Copyright (c) 2012 Netresearch GmbH & Co. KG (http://www.netresearch.de/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Hermes API client unittest
 *
 * @category    Netresearch
 * @package     Netresearch_Hermes
 * @author      Michael Lühr <michael.luehr@netresearch.de>
 */
class Netresearch_Hermes_Block_Adminhtml_Sales_Order_Shipment_View_Tracking 
    extends Dhl_Intraship_Block_Adminhtml_Sales_Order_Shipment_View_Tracking
{

    /**
     * Extend tracking block to attach hermes form.
     *
     * @return string
     */
    public function _toHtml()
    {
        if (false === Mage::getModel('hermes/config')->isEnabled()) {
            return parent::_toHtml();
        }

        $block = $this->getLayout()->createBlock(
            'hermes/adminhtml_sales_order_shipment_create_hermes',
            'hermes_view', array('template' => 'hermes/sales/order/shipment/view/hermes.phtml'));
        return parent::_toHtml() . $block->_toHtml();
    }
}
