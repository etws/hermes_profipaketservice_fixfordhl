<?php

class Netresearch_Hermes_Test_Controller_PermissionsDeniedControllerTest
    extends Netresearch_Hermes_Test_Controller_AbstractControllerTest
{
    public function setUp()
    {
        $this->reset();
        $this->_fakeLogin();
        parent::setUp();
    }
    
    protected function _allowGlobalAccess()
    {
        $session = $this->getModelMock('admin/session', array('isAllowed'));
        $session->expects($this->any())
            ->method('isAllowed')
            ->will($this->returnCallback(array($this, 'fakePermission')));
//            ->will($this->returnValue(false));
        $this->replaceByMock('singleton', 'admin/session', $session);
    }

    public static function fakePermission($path)
    {
        return 'sales/shipment' === $path;
    }

    public function testCheckSectionAllowed()
    {
        $this->dispatch('adminhtml/parcel/transmitHermesParcels');
        $this->assertRedirect();
        Mage::getSingleton('adminhtml/session')->getMessages(true);
    }
    
    
}
